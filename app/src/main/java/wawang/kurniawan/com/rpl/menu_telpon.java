package wawang.kurniawan.com.rpl;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class menu_telpon extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_telpon);
    }
    public void masuksms(View view){
        Intent i = new Intent(getApplicationContext(), detai_sms.class);
        startActivity(i);
    }
    public void masuktlp(View view){
        Intent in = new Intent(getApplicationContext(), detail_tlp.class);
        startActivity(in);
    }
    public void masukalm(View view){
        Intent inte = new Intent(getApplicationContext(), detai_alm.class);
        startActivity(inte);
    }
    public void masukcamera(View view){
        Intent inten = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(inten,0);
    }
    public void masukmusic(View view){
        Intent intent = new Intent(getApplicationContext(), detail_music.class);
        startActivity(intent);
    }
    public void masukcuaca(View view){
        Intent intent1 = new Intent(getApplicationContext(), detail_cuaca.class);
        startActivity(intent1);
    }
    public void masukmail(View view){
        Intent intent2 = new Intent(getApplicationContext(), detail_mail.class);
        startActivity(intent2);
    }
    public void masukkalender(View view){
        Intent intent3 = new Intent(getApplicationContext(), detail_calender.class);
        startActivity(intent3);
    }
    public void masukgaleri(View view){
        Intent intent4 = new Intent(getApplicationContext(), detail_galery.class);
        startActivity(intent4);
    }
    public void masukblue(View view){
        Intent intent5 = new Intent(getApplicationContext(), detail_blue.class);
        startActivity(intent5);
    }
    public void masukjalan(View view){
        Intent intent6 = new Intent(getApplicationContext(), detail_pedo.class);
        startActivity(intent6);
    }
    public void masukhearth(View view){
        Intent intent7 = new Intent(getApplicationContext(), detail_hearth.class);
        startActivity(intent7);
    }
}
